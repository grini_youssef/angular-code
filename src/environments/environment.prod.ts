export const environment = {
  production: true,
  baseUrl: 'https://back-entreprise-grini-youssef.cloud.okteto.net',
  keycloak_url: 'https://keycloak-grini-youssef.cloud.okteto.net/auth',
  parsing_url: 'https://parsing-grini-youssef.cloud.okteto.net/model'
  
};