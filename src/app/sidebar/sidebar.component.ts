import { Component, OnInit } from "@angular/core";
import { Router, Event, NavigationEnd } from "@angular/router";
import { AllModulesService } from "../all-modules/all-modules.service";
import { EntrepriseConfigService } from "../services/entreprise-config.service";
import { EntrepriseService } from "../services/entreprise.service";
import { KeycloakSecurityService } from "../services/keycloak-security.service";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  urlComplete = {
    mainUrl: "",
    subUrl: "",
    childUrl: "",
  };

  sidebarMenus = {
    default: true,
    chat: false,
    settings: false,
  };

  members = {};
  groups = {};

  isAuth = false;
  keycloak: any;
  isAccepted: boolean;

  pathsInterview: string[]=['list-offers','workflow-interviews','interviews-by-offer'];

  constructor(
    private router: Router,
    private allModulesService: AllModulesService,
    private keycloakSecurityService: KeycloakSecurityService,
    private entrepriseConfigService: EntrepriseConfigService
  ) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        $(".main-wrapper").removeClass('slide-nav');
        $(".sidebar-overlay").removeClass('opened');
        const url = event.url.split("/");
        console.log(url);
        this.urlComplete.mainUrl = url[2];
        this.urlComplete.subUrl = url[3];
        this.urlComplete.childUrl = url[4];
        if (url[2] === "entreprise") {
          this.urlComplete.mainUrl = "dashboard";
          this.urlComplete.subUrl = "admin";
        }

        if (url[3] === "chat" || url[3] === "calls") {
          this.sidebarMenus.chat = true;
          this.sidebarMenus.default = false;
        } else {
          this.sidebarMenus.chat = false;
          this.sidebarMenus.default = true;
        }
      }
    });

    this.groups = { ...this.allModulesService.groups };
    this.members = { ...this.allModulesService.members };
  }

  ngOnInit() {
    this.keycloak = this.keycloakSecurityService.keycloak;
    this.isAuth = this.keycloak.authenticated;
    if(this.isAuth){
      this.entrepriseConfigService.init()
      .then(data => {
        this.isAccepted = (data == "accepted") ;
  
      }).catch(err => {
        console.error('err', err);
        this.isAccepted = false;
      });
    }

    // Slide up and down of menus
    $(document).on("click", "#sidebar-menu a", function (e) {
      e.stopImmediatePropagation();
      if ($(this).parent().hasClass("submenu")) {
        e.preventDefault();
      }
      if (!$(this).hasClass("subdrop")) {
        $("ul", $(this).parents("ul:first")).slideUp(350);
        $("a", $(this).parents("ul:first")).removeClass("subdrop");
        $(this).next("ul").slideDown(350);
        $(this).addClass("subdrop");
      } else if ($(this).hasClass("subdrop")) {
        $(this).removeClass("subdrop");
        $(this).next("ul").slideUp(350);
      }
    });
  }

  setActive(member) {
    this.allModulesService.members.active = member;
  }

  isEntrepriseManager() {
    return this.keycloak.hasRealmRole('entreprise-manager');
  }

  isEntrepriseHR() {
    return this.keycloak.hasRealmRole('hr-assistant');
  }
}
