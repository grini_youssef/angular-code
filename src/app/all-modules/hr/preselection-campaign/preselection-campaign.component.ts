import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { Candidature } from 'src/app/models/candidature';
import { Certificat } from 'src/app/models/certificat';
import { Critere } from 'src/app/models/critere';
import { Diplome } from 'src/app/models/diplome';
import { Experiance } from 'src/app/models/experience';
import { Offer } from 'src/app/models/offer';
import { Skill } from 'src/app/models/skill';
import { CandidatureService } from 'src/app/services/candidature.service';
import { CritereService } from 'src/app/services/critere.service';
import { OfferService } from 'src/app/services/offer.service';
import { ParsingService } from 'src/app/services/parsing.service';
import {ThemePalette} from '@angular/material/core';
import {ProgressBarMode} from '@angular/material/progress-bar';
declare var $:any;


@Component({
  selector: 'app-preselection-campaign',
  templateUrl: './preselection-campaign.component.html',
  styleUrls: ['./preselection-campaign.component.scss']
})
export class PreselectionCampaignComponent implements OnInit, OnDestroy {

  offer = new  Offer();
  errorMessage: string = null;
  criteres : any = [];
  critere = new Critere();
  candidature = new Candidature();
  candidaturesList : Array<Candidature> = [];
  public allCandidatures: any = [];
  dim = 0;
  id_offer :any;
  progress : Boolean = false;

  public lstFileList: any[];
  public lstSearchList: any[];

  color: ThemePalette = 'primary';
  mode: ProgressBarMode = 'indeterminate';


  @ViewChild(DataTableDirective, { static: false })
  public dtElement: DataTableDirective;
  public dtOptions: DataTables.Settings = {};
  public editId: any;
  public tempIdCan: any;
  public rows = [];
  public srch = [];
  public dtTrigger: Subject<any> = new Subject();


  constructor(
    private route: ActivatedRoute,
    private candidatureService: CandidatureService,
    private toastr: ToastrService,
    private parsingService : ParsingService,
    private offerService : OfferService,
    private router: Router,
    private critereService : CritereService,
  ) { }

  criteresList = ["Java", "Kubernetes", "Keycloak", "python"];
  pourcentage = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

  statusList = [
    { value: 'pending', label: 'pending'},
    { value: 'approved', label: 'approve'}
  ];


  ngOnInit(): void {
    this.id_offer = this.route.snapshot.params['id'];
    this.getOffer();
    this.getCriteres();


    $(".floating")
    .on("focus blur", function (e) {
      $(this)
        .parents(".form-focus")
        .toggleClass("focused", e.type === "focus" || this.value.length > 0);
    })
    .trigger("blur");

    /*
    $(document).on('click', '#upload_btn', function () {
      $(".progress").css('visibility','visible');
      $(".progress-bar").css({"width":"100%","transition": "60s"});
    });
    */

  this.getCandidatures();


  // for data table configuration
  this.dtOptions = {
    // ... skipped ...
    pageLength: 10,
    dom: "lrtip",
  };

  }

  // manually rendering Data table

  rerender(): void {
    $("#datatable").DataTable().clear();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
    this.allCandidatures = [];
    this.getCandidatures();
    setTimeout(() => {
      this.dtTrigger.next();
    }, 1000);
  }



  getOffer() {
    this.offerService.getOffer(this.id_offer).subscribe(
          data => {
            this.offer = data;
          },
          err => {
            this.errorMessage = err;
            console.log('error getting offer !', err.error.message);
          })
  }

  getCandidature() {
    this.candidatureService.getCandidature(this.editId).subscribe(
          data => {
            this.candidature = data;
          },
          err => {
            this.errorMessage = err;
            console.log('error getting candidature !', err.error.message);
          })
  }


  getCandidatures() {
    this.candidatureService.getCandidatures(this.id_offer).subscribe((data) => {
      this.allCandidatures = data;
      console.log("===getCandidatures():===");
      console.log(this.allCandidatures);
      this.dtTrigger.next();
      if(data != null){
        this.rows = this.allCandidatures;
        this.srch = [...this.rows];
      }
      
    });
  }


  async uploadCvs(){
    
    const headers = new HttpHeaders();
    headers.append('enctype', 'multipart/form-data');
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('X-requested-with', 'XMLHttpRequest');

    let el = document.getElementById('dataTable');
    el.scrollIntoView();

    const formData = new FormData();
    if (this.lstFileList.length > 0) {
      this.progress = true;
      for (const row of this.lstFileList) {
        this.toastr.info("Wait ...", "still "+(this.lstFileList.length - this.dim)+" resumes");
        formData.append('files', row);
        await this.parseOnePromise(formData).then(data => {
          this.dim = this.dim + 1 ;
          }).catch(err => {
        });
        formData.delete('files');
      }
      //this.parseMultiple(formData);
    }
  }



  parseOnePromise(formData){
    return new Promise((resolve, reject) => {

      this.parsingService.parse(formData).subscribe(
        data => {
          let candidatureRes : any;
          candidatureRes = data[0];
          console.log("===Parsing===")
          console.log(data);
          let candidature = new Candidature();
          candidature.firstName = candidatureRes.first_name != null ? candidatureRes.first_name : null;
          candidature.lastName = candidatureRes.last_name != null ? candidatureRes.last_name : null;
          candidature.email = candidatureRes.email != null ? candidatureRes.email : null;
          candidature.phone = candidatureRes.phone_number != null ? candidatureRes.phone_number.replace(/\s/g, ""): null;
          candidature.city = candidatureRes.city != null ? candidatureRes.city : null;
  
          candidature.status = "pending";
          candidature.score = 0;
  
          if (candidatureRes.diplomas){
            for (var j = 0; j < candidatureRes.diplomas.length; j++){
              let diplome = new Diplome();
              diplome.name = candidatureRes.diplomas[j].name != null ? candidatureRes.diplomas[j].name : null;
              diplome.school = candidatureRes.diplomas[j].school != null ? candidatureRes.diplomas[j].school : null;
              if(candidatureRes.diplomas[j].year){
                diplome.dateGraduation = new Date(candidatureRes.diplomas[j].year);
              }
  
              candidature.diplomes.push(diplome);
            }
          }
          
  
          if(candidatureRes.experiences){
            for (var j = 0; j < candidatureRes.experiences.length; j++){
              let experience = new Experiance();
              experience.title = candidatureRes.experiences[j].title != null ? candidatureRes.experiences[j].title : null;
              experience.company = candidatureRes.experiences[j].company != null ? candidatureRes.experiences[j].company : null;
              experience.tasks = candidatureRes.experiences[j].tasks != null ? candidatureRes.experiences[j].tasks : null;
              
              if (candidatureRes.experiences[j].date){
                let date = candidatureRes.experiences[j].date.split(' ');
                experience.dateFrom = new Date (date[0]);
                experience.dateTo = new Date (date[1]);
              }
  
              candidature.experiences.push(experience);
            }
          }
          
  
          if(candidatureRes.skills){
            for (var j = 0; j < candidatureRes.skills.length; j++){
              let skill = new Skill();
              skill.name = candidatureRes.skills[j] != null ? candidatureRes.skills[j] : null;
  
              candidature.skills.push(skill);
            }
          }
          
  
          if(candidatureRes.certifications){
            for (var j = 0; j < candidatureRes.certifications.length; j++){
              let certificat = new Certificat();
              certificat.name = candidatureRes.certifications[j] != null ? candidatureRes.certifications[j] : null;
  
              candidature.certificats.push(certificat);
            }
          }
          this.candidaturesList.push(candidature);
          this.CreateOneCandidature();
          this.candidaturesList = [];
          resolve(true);
        },
        err => {
          console.log('error parsing ! ')
          console.log(err);
          reject(false);
      }) 

    });
    
  }

  parseOne(formData){
    this.parsingService.parse(formData).subscribe(
      data => {
        let candidatureRes : any;
        candidatureRes = data[0];
        console.log("===Parsing===")
        console.log(data);
        let candidature = new Candidature();
        candidature.firstName = candidatureRes.first_name != null ? candidatureRes.first_name : null;
        candidature.lastName = candidatureRes.last_name != null ? candidatureRes.last_name : null;
        candidature.email = candidatureRes.email != null ? candidatureRes.email : null;
        candidature.phone = candidatureRes.phone_number != null ? candidatureRes.phone_number.replace(/\s/g, ""): null;
        candidature.city = candidatureRes.city != null ? candidatureRes.city : null;

        candidature.status = "pending";
        candidature.score = 0;

        if (candidatureRes.diplomas){
          for (var j = 0; j < candidatureRes.diplomas.length; j++){
            let diplome = new Diplome();
            diplome.name = candidatureRes.diplomas[j].name != null ? candidatureRes.diplomas[j].name : null;
            diplome.school = candidatureRes.diplomas[j].school != null ? candidatureRes.diplomas[j].school : null;
            if(candidatureRes.diplomas[j].year){
              diplome.dateGraduation = new Date(candidatureRes.diplomas[j].year);
            }

            candidature.diplomes.push(diplome);
          }
        }
        

        if(candidatureRes.experiences){
          for (var j = 0; j < candidatureRes.experiences.length; j++){
            let experience = new Experiance();
            experience.title = candidatureRes.experiences[j].title != null ? candidatureRes.experiences[j].title : null;
            experience.company = candidatureRes.experiences[j].company != null ? candidatureRes.experiences[j].company : null;
            experience.tasks = candidatureRes.experiences[j].tasks != null ? candidatureRes.experiences[j].tasks : null;
            
            if (candidatureRes.experiences[j].date){
              let date = candidatureRes.experiences[j].date.split(' ');
              experience.dateFrom = new Date (date[0]);
              experience.dateTo = new Date (date[1]);
            }

            candidature.experiences.push(experience);
          }
        }
        

        if(candidatureRes.skills){
          for (var j = 0; j < candidatureRes.skills.length; j++){
            let skill = new Skill();
            skill.name = candidatureRes.skills[j] != null ? candidatureRes.skills[j] : null;

            candidature.skills.push(skill);
          }
        }
        

        if(candidatureRes.certifications){
          for (var j = 0; j < candidatureRes.certifications.length; j++){
            let certificat = new Certificat();
            certificat.name = candidatureRes.certifications[j] != null ? candidatureRes.certifications[j] : null;

            candidature.certificats.push(certificat);
          }
        }
        this.candidaturesList.push(candidature);
        this.CreateOneCandidature();
        this.candidaturesList = [];
      },
      err => {
        console.log('error parsing ! ')
        console.log(err);
    }) 
  }

  parseMultiple(formData){
    this.parsingService.parse(formData).subscribe(
      data => {
        let candidatures : any;
        candidatures = data;
        console.log("===Parsing===")
        console.log(data);
        var i;
        for(i = 0; i < candidatures.length; i++){
          let candidature = new Candidature();
          candidature.firstName = candidatures[i].first_name != null ? candidatures[i].first_name : null;
          candidature.lastName = candidatures[i].last_name != null ? candidatures[i].last_name : null;
          candidature.email = candidatures[i].email != null ? candidatures[i].email : null;
          candidature.phone = candidatures[i].phone_number != null ? candidatures[i].phone_number.replace(/\s/g, ""): null;
          candidature.city = candidatures[i].city != null ? candidatures[i].city : null;

          candidature.status = "pending";
          candidature.score = 0;

          if (candidatures[i].diplomas){
            for (var j = 0; j < candidatures[i].diplomas.length; j++){
              let diplome = new Diplome();
              diplome.name = candidatures[i].diplomas[j].name != null ? candidatures[i].diplomas[j].name : null;
              diplome.school = candidatures[i].diplomas[j].school != null ? candidatures[i].diplomas[j].school : null;
              if(candidatures[i].diplomas[j].year){
                diplome.dateGraduation = new Date(candidatures[i].diplomas[j].year);
              }

              candidature.diplomes.push(diplome);
            }
          }
          

          if(candidatures[i].experiences){
            for (var j = 0; j < candidatures[i].experiences.length; j++){
              let experience = new Experiance();
              experience.title = candidatures[i].experiences[j].title != null ? candidatures[i].experiences[j].title : null;
              experience.company = candidatures[i].experiences[j].company != null ? candidatures[i].experiences[j].company : null;
              experience.tasks = candidatures[i].experiences[j].tasks != null ? candidatures[i].experiences[j].tasks : null;
              
              if (candidatures[i].experiences[j].date){
                let date = candidatures[i].experiences[j].date.split(' ');
                experience.dateFrom = new Date (date[0]);
                experience.dateTo = new Date (date[1]);
              }

              candidature.experiences.push(experience);
            }
          }
          

          if(candidatures[i].skills){
            for (var j = 0; j < candidatures[i].skills.length; j++){
              let skill = new Skill();
              skill.name = candidatures[i].skills[j] != null ? candidatures[i].skills[j] : null;

              candidature.skills.push(skill);
            }
          }
          

          if(candidatures[i].certifications){
            for (var j = 0; j < candidatures[i].certifications.length; j++){
              let certificat = new Certificat();
              certificat.name = candidatures[i].certifications[j] != null ? candidatures[i].certifications[j] : null;

              candidature.certificats.push(certificat);
            }
          }
          
          this.candidaturesList.push(candidature);
        }
        this.CreateCandidatures();
      },
      err => {
        console.log('error parsing ! ')
        console.log(err);
    })
  }

  CreateOneCandidature() {
    let dataRefreshed = false;
    this.candidatureService.CreateCandidatures(this.offer.id, this.candidaturesList).subscribe((data) => {
      //this.router.navigate(["/entreprise/hr/preselection-campaign/"+this.id_offer]);
      if(this.dim % 10 === 0){
        this.getCandidatures();
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
        dataRefreshed = true;
      }
      
      if(this.lstFileList.length == this.dim){
        this.progress = false;
        this.dim = 0;
        this.toastr.success("Parsing finished.", "Success");
        if(!dataRefreshed){
          this.getCandidatures();
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
          });
        }
      }
    });
  }

  CreateCandidatures() {
    this.candidatureService.CreateCandidatures(this.offer.id, this.candidaturesList).subscribe((data) => {
      //this.router.navigate(["/entreprise/hr/preselection-campaign/"+this.id_offer]);
      this.getCandidatures();
      this.toastr.success("Parsing finished.", "Success");
      this.progress = false;
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
  }

  deleteCandidature() {
    this.candidatureService.DeleteCandidature(this.tempIdCan).subscribe((data) => {
      this.router.navigate(["/entreprise/hr/preselection-campaign/"+this.id_offer]);
      this.getCandidatures();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
    $("#delete_candidature").modal("hide");
    this.toastr.success("Candidature deleted", "Success");
  }

  onFolderSelected(event){
    if (event.target.files.length > 0){
        let files = event.target.files;
        this.lstFileList = files;
        this.lstSearchList = files;
        //this.progress = true;
        console.log("===onFolderSelected===")
        console.log(this.lstFileList);
    }      
  }

  getCriteres() {
    this.critereService.getCriteres(this.id_offer).subscribe(
          data => {
            this.criteres = data;
          },
          err => {
            this.errorMessage = err;
            console.log('error getting criters !');
            console.log(err);
          })
  }

  CreateCritere() {
    this.critereService.CreateCritere(this.id_offer, this.critere).subscribe((data) => {
      this.router.navigate(["/entreprise/hr/preselection-campaign/"+this.id_offer]);
      this.getCriteres();
      this.getCandidatures();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
    $("#add_critere").modal("hide");
    this.critere = new Critere();
    this.toastr.success("Critere added", "Success");
  }


  deleteCritere(id) {
    this.critereService.DeleteCritere(id).subscribe((data) => {
      this.router.navigate(["/entreprise/hr/preselection-campaign/"+this.id_offer]);
      this.getCriteres();
      this.getCandidatures();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
    this.toastr.success("Critere deleted", "Success");
  }


  // search filter in files and docs
  searchCV(searchValue: string) {
    this.lstSearchList = [...this.lstFileList].filter(res => {
      return res.name.toLowerCase().includes(searchValue.toLowerCase());
    });
  }

  viewCV(name){
    var i;
    for (i = 0; i < this.lstFileList.length; i++) {
          if (this.lstFileList[i].name === name) {
              break;
          }
    }
    console.log("=== View Cv ===");
    console.log(this.lstFileList[i]);
    
    var reader = new FileReader();
    reader.readAsDataURL(this.lstFileList[i]);

    reader.onload = (e: any) => {
      //console.log('csv content', reader.result);
    };
    //window.open("D:\time2it\cv\CV NACIRI Youssef.pdf", null);
  }

  deleteCV(name){
    this.lstFileList = [...this.lstFileList].filter(r => r.name != name);
    this.lstSearchList = this.lstFileList;
    this.toastr.success(name+" deleted", "Success");
  }


  // Edit Status Modal Api Call

  editStatus() {
    this.candidatureService.UpdateCandidatureStatus(this.editId, this.candidature).subscribe((data1) => {
      this.router.navigate(["/entreprise/hr/preselection-campaign/"+this.id_offer]);
      this.getCandidatures();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
    
    $("#edit_status").modal("hide");
    this.toastr.success("Status edited", "Success");
  }

  
  edit(value) {
    this.editId = value;
    const index = this.allCandidatures.findIndex((item) => {
      return item.id === value;
    });
    let toSetValues = this.allCandidatures[index];
    this.candidature = toSetValues;
  }



  //search by name
  searchName(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return (d.firstName.toLowerCase().indexOf(val) !== -1 || !val) || (d.lastName.toLowerCase().indexOf(val) !== -1 || !val);
    });
    this.rows.push(...temp);
  }

  //search by name
  searchCity(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.city.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by status

  searchStatus(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.status.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by min experiance

  searchMinExp(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      return d.yearsExp >= val  || !val;
    });
    this.rows.push(...temp);
  }

  //search by max experiance

  searchMaxExp(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      return d.nbre_annee_exp <= val  || !val;
    });
    this.rows.push(...temp);
  }


  //for unsubscribe datatable
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
