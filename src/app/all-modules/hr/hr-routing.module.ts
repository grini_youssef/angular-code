import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HrComponent } from './hr.component';
import { PreselectionCampaignsComponent } from './preselection-campaigns/preselection-campaigns.component';
import { AfterLoginEntrepriseHrAcceptedService } from 'src/app/services/guards/after-login-entreprise-hr-accepted.service';
import { PreselectionCampaignComponent } from './preselection-campaign/preselection-campaign.component';
import { CandidatureComponent } from './candidature/candidature.component';
import { InterviewCampaignsComponent } from './interview-campaigns/interview-campaigns.component';
import { OffersComponent } from './offers/offers.component';
import { OfferDetailsComponent } from './offers/offer-details/offer-details.component';
import { CandidaturesComponent } from './candidatures/candidatures.component';


const routes: Routes = [
  {
    path:"",
    component:HrComponent,
    children:[
      {path: '', redirectTo: 'offers', pathMatch:'full'},
      {
        path:"preselection-campaigns",
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component:PreselectionCampaignsComponent
      },
      {
        path:"interview-campaigns",
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component:InterviewCampaignsComponent
      },
      {
        path:"preselection-campaign/:id",
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component:PreselectionCampaignComponent
      },
      {
        path:"candidature/:id",
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component:CandidatureComponent
      },
      {
        path:"offers",
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component:OffersComponent
      },
      {
        path:"offer-details/:id",
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component:OfferDetailsComponent
      },
      {
        path:"candidatures",
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component:CandidaturesComponent
      },
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HrRoutingModule { }
