import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HrRoutingModule } from './hr-routing.module';
import { HrComponent } from './hr.component';

import { DataTablesModule } from 'angular-datatables';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SharingModule } from 'src/app/sharing/sharing.module';
import { PreselectionCampaignsComponent } from './preselection-campaigns/preselection-campaigns.component';
import { PreselectionCampaignComponent } from './preselection-campaign/preselection-campaign.component';
import { CandidatureComponent } from './candidature/candidature.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { InterviewCampaignsComponent } from './interview-campaigns/interview-campaigns.component';
import { OffersComponent } from './offers/offers.component';
import { OfferDetailsComponent } from './offers/offer-details/offer-details.component';
import { CandidaturesComponent } from './candidatures/candidatures.component';


@NgModule({
  declarations: [
    HrComponent, 
    PreselectionCampaignsComponent,
    InterviewCampaignsComponent,
    PreselectionCampaignComponent, 
    CandidatureComponent,
    OffersComponent,
    OfferDetailsComponent,
    CandidaturesComponent,
  ],
  imports: [
    CommonModule,
    HrRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    SharingModule,
    MatProgressBarModule,
  ]
})
export class HrModule { }
