import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { OfferService } from 'src/app/services/offer.service';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
declare var $:any;


@Component({
  selector: 'app-candidatures',
  templateUrl: './candidatures.component.html',
  styleUrls: ['./candidatures.component.scss']
})
export class CandidaturesComponent implements OnInit, OnDestroy {

  public allOffers:any = [];
  id_entreprise :any;



  @ViewChild(DataTableDirective, { static: false })
  public dtElement: DataTableDirective;
  public dtOptions: DataTables.Settings = {};


  public rows = [];
  public srch = [];
  public dtTrigger: Subject<any> = new Subject();


  constructor(
    private offerService : OfferService,
    private keycloakSecurityService : KeycloakSecurityService,
  ) { }


  ngOnInit(): void {
    this.id_entreprise = this.keycloakSecurityService.userInformations.entreprise;

    $(".floating")
    .on("focus blur", function (e) {
      $(this)
        .parents(".form-focus")
        .toggleClass("focused", e.type === "focus" || this.value.length > 0);
    })
    .trigger("blur");

  this.getOffers();


  // for data table configuration
  this.dtOptions = {
    // ... skipped ...
    pageLength: 10,
    dom: "lrtip",
  };

  }

  // manually rendering Data table

  rerender(): void {
    $("#datatable").DataTable().clear();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
    this.allOffers = [];
    this.getOffers();
    setTimeout(() => {
      this.dtTrigger.next();
    }, 1000);
  }



  getOffers() {
    this.offerService.getOffers(this.id_entreprise).subscribe((data) => {
      this.allOffers = data;
      this.dtTrigger.next();
      if(data != null){
        this.rows = this.allOffers;
        this.srch = [...this.rows];
        console.log(this.allOffers);
      }
      
    });
  }


  //search by name
  searchOffer(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.title.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by name
  searchName(val) {
    this.rows.splice(0, this.rows.length);
    for(let i = 0 ; i<this.allOffers.length; i++){
        this.srch["candidatures"]
        let temp = this.srch[i]["candidatures"].filter(function (d) {
          val = val.toLowerCase();
          
          return d.firstName.toLowerCase().indexOf(val) !== -1 || !val;
          
        });
        this.rows.push(...temp);
    }
  }

  //search by name
  searchCity(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.city.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by status

  searchStatus(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.status.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by min experiance

  searchMinExp(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      return d.yearsExp >= val  || !val;
    });
    this.rows.push(...temp);
  }

  //search by max experiance

  searchMaxExp(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      return d.nbre_annee_exp <= val  || !val;
    });
    this.rows.push(...temp);
  }


  //for unsubscribe datatable
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
