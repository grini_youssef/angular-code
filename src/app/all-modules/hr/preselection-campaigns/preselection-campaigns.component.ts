import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
} from "@angular/core";

import { Subject } from "rxjs";
import { DataTableDirective } from "angular-datatables";
import { KeycloakSecurityService } from "src/app/services/keycloak-security.service";
import { OfferService } from "src/app/services/offer.service";

declare const $: any;
@Component({
  selector: "app-preselection-campaigns",
  templateUrl: "./preselection-campaigns.component.html",
  styleUrls: ["./preselection-campaigns.component.css"],
})
export class PreselectionCampaignsComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false })
  public dtElement: DataTableDirective;
  public dtOptions: DataTables.Settings = {};

  public allCampaigns: any = [];

  public tempId: any;
  public rows = [];
  public srch = [];
  public dtTrigger: Subject<any> = new Subject();


  errorMessage: any;
  id_entreprise: any;

  
  constructor(
    private keycloakSecurityService : KeycloakSecurityService,
    private offerService : OfferService,
  ) {}

  ngOnInit() { 
    $(".floating")
      .on("focus blur", function (e) {
        $(this)
          .parents(".form-focus")
          .toggleClass("focused", e.type === "focus" || this.value.length > 0);
      })
      .trigger("blur");
    
    this.id_entreprise = this.keycloakSecurityService.userInformations.entreprise;
    this.getCampaigns();


    // for data table configuration
    this.dtOptions = {
      // ... skipped ... 
      pageLength: 10,
      dom: "lrtip",
    };
  }



  // manually rendering Data table

  rerender(): void {
    $("#datatable").DataTable().clear();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
    this.allCampaigns = [];
    this.getCampaigns();
    setTimeout(() => {
      this.dtTrigger.next();
    }, 1000);
  }

  getCampaigns() {
    this.offerService.getOffers(this.id_entreprise).subscribe((data) => {
      this.allCampaigns = data;
      this.dtTrigger.next();
      if(data != null){
        this.rows = this.allCampaigns;
        this.srch = [...this.rows];
      }
      
    });
  }



  //search by title 
  searchTitle(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.title.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }



  //for unsubscribe datatable
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}
