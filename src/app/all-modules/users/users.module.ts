import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { EntrepriseUsersComponent } from './entreprise-users/entreprise-users.component';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateEntrepriseUserComponent } from './entreprise-users/create-entreprise-user/create-entreprise-user.component';
import { EditEntrepriseUserComponent } from './entreprise-users/edit-entreprise-user/edit-entreprise-user.component';
import { SharingModule } from 'src/app/sharing/sharing.module';

@NgModule({
  declarations: [UsersComponent, EntrepriseUsersComponent, CreateEntrepriseUserComponent, EditEntrepriseUserComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    SharingModule
  ]
})
export class UsersModule { }
