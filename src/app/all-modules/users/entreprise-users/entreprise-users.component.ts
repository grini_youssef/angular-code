import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Subject } from "rxjs";
import { DataTableDirective } from "angular-datatables";
import { EntrepriseService } from "src/app/services/entreprise.service";
import { KeycloakSecurityService } from "src/app/services/keycloak-security.service";
import { RoleService } from "src/app/services/role.service";
import { Role } from "src/app/models/role";
import { Entreprise } from "src/app/models/entreprise";
import { User } from "src/app/models/user";
import { Router } from "@angular/router";

declare const $: any;
@Component({
  selector: "app-entreprise-users",
  templateUrl: "./entreprise-users.component.html",
  styleUrls: ["./entreprise-users.component.css"],
})
export class EntrepriseUsersComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false })
  public dtElement: DataTableDirective;
  public dtOptions: DataTables.Settings = {};
  public allUsers: any = [];
  errorMessage: any;
  public editId: any;
  public tempId: any;
  public rows = [];
  public srch = [];
  public dtTrigger: Subject<any> = new Subject();

  entreprise = new Entreprise();
  id_entreprise : any;
  user_roles : Array<any> = [];
  user = new User();
  roles : any;

  constructor(
    private entrepriseService : EntrepriseService,
    private keycloakSecurityService : KeycloakSecurityService,
    private roleService : RoleService,
    private toastr: ToastrService,
    private router: Router,
  ) {}

  ngOnInit() {
    
    $(".floating")
      .on("focus blur", function (e) {
        $(this)
          .parents(".form-focus")
          .toggleClass("focused", e.type === "focus" || this.value.length > 0);
      })
      .trigger("blur");
      

    this.id_entreprise = this.keycloakSecurityService.userInformations.entreprise;
    this.getUsers();
    

    // for data table configuration
    this.dtOptions = {
      // ... skipped ...
      pageLength: 10,
      dom: "lrtip",
    };
  }


  // manually rendering Data table

  rerender(): void {
    $("#datatable").DataTable().clear();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
    this.allUsers = [];
    this.getUsers();
    setTimeout(() => {
      this.dtTrigger.next();
    }, 1000);
  }

  getUsers() {
    this.entrepriseService
    .getEntrepriseUsers(this.id_entreprise).subscribe((data) => {
        this.allUsers = data;
        this.removeAdmin();
        this.dtTrigger.next();
        this.rows = this.allUsers;
        this.srch = [...this.rows];
      });
  }

  removeAdmin(){
    let admin = this.keycloakSecurityService.userInformations.preferred_username;
    this.allUsers = this.allUsers.filter(u => u.username != admin);
  }



  getUserRoles(id){
    this.roleService.getUserRoles(id).subscribe(
      data => {
        this.user_roles = data;
      },
      err => {
        this.errorMessage = err;
        console.log('error getting user roles ! ', err)
      }
    );
  }


  // Delete Provident Modal Api Call

  deleteUser() {
    this.entrepriseService.DeleteUser(this.tempId).subscribe((data) => {
      this.router.navigate(["/entreprise/users/entreprise-users"]);
      this.getUsers();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
    $("#delete_user").modal("hide");
    this.toastr.success("User deleted with success", "Success");
  } 


  //search by username
  searchUserName(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.username.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }




  //search by fisrt name
  searchFirstName(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.firstName.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by fisrt name
  searchLastName(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      return d.lastName.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
