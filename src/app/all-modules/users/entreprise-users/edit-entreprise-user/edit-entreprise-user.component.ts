import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Entreprise } from "src/app/models/entreprise";
import { User } from "src/app/models/user";
import { EntrepriseService } from "src/app/services/entreprise.service";
import { KeycloakSecurityService } from "src/app/services/keycloak-security.service";
import { RoleService } from "src/app/services/role.service";
import { Role } from "src/app/models/role";

@Component({
  selector: "app-edit-entreprise-user",
  templateUrl: "./edit-entreprise-user.component.html",
  styleUrls: ["./edit-entreprise-user.component.css"],
})
export class EditEntrepriseUserComponent implements OnInit {
  public id;

  entreprise = new Entreprise();
  id_entreprise : any;
  user_roles : Array<any> = [];
  user = new User();
  roles : any;
  errorMessage: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,

    private entrepriseService : EntrepriseService,
    private keycloakSecurityService : KeycloakSecurityService,
    private roleService : RoleService,
  ) {}

  ngOnInit() {
    //getting edit id of selected estimate list
    this.id = this.route.snapshot.params['id'];

    //getting user
    this.getUser();

    //getting all roles
    this.getRoles();

    //getting user roles
    this.getUserRoles();
  }


  //get User
  getUser(){
    this.entrepriseService.getUser(this.id).subscribe(
      data => {
        this.user = data;
      },
      err => {
        this.errorMessage = err;
        console.log('error getting user ! ', err)
      }
    );
  }


  //get all roles
  getRoles(){
    this.roleService.getRoles().subscribe(
      data => {
        this.roles = data;
        this.roles = this.roles.filter(r => r.name != "app-manager");
      },
      err => {
        this.errorMessage = err;
        console.log('error getting roles ! ', err);
      }
    );
  }

  //get user roles
  getUserRoles(){
    this.roleService.getUserRoles(this.id).subscribe(
      data => {
        this.user_roles = data;
      },
      err => {
        this.errorMessage = err;
        console.log('error getting user roles ! ', err)
      }
    );
  }

  onChange(role:Role, isChecked: boolean) {
    if(isChecked) {
      this.user_roles.push(role);
    } else {
        var i;
        for (i = 0; i < this.user_roles.length; i++) {
          if (this.user_roles[i].id === role.id) {
              break;
          }
      }
      this.user_roles.splice(i,1);
    }
  }

  toggleCheckBox(role:Role){
    var i;
    for (i = 0; i < this.user_roles.length; i++) {
        if (this.user_roles[i].id === role.id) {
            return true;
        }
    }
    return false;
  }


  updateUser() {
    this.toastr.info("Wait ...","Info",{
      disableTimeOut: false,
      tapToDismiss: true,
      closeButton: true,
    });
    this.entreprise.id = this.keycloakSecurityService.userInformations.entreprise;
    this.entrepriseService
        .UpdateUser(this.id,this.user, this.entreprise, this.user_roles).subscribe(
          response => {
            console.log(response);
            if(response["message"] == "Email already exists !!"){
              this.toastr.clear();
              this.toastr.warning(response["message"], "Warning!");
            }
            else{
              this.toastr.clear();
              this.toastr.success(response["message"], "Success");
              this.router.navigate(["/entreprise/users/entreprise-users"]);
            }
          },
          err => {
            this.errorMessage = err;
            this.toastr.clear();
            this.toastr.warning(err.error.message, "Warning!");
          });
  }


}
