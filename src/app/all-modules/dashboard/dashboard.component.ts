import { Component, OnInit, HostListener, NgZone } from "@angular/core";
import { Router, Event, NavigationEnd } from "@angular/router";
import { EntrepriseConfigService } from "src/app/services/entreprise-config.service";
import { EntrepriseService } from "src/app/services/entreprise.service";
import { KeycloakSecurityService } from "src/app/services/keycloak-security.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"],
})
@HostListener("window: resize", ["$event"])
export class DashboardComponent implements OnInit {
  public innerHeight: any;

  keycloak: any;

  getScreenHeight() {
    this.innerHeight = window.innerHeight + "px";
  }

  constructor(
    private ngZone: NgZone, 
    private router: Router,
    private keycloakSecurityService: KeycloakSecurityService,
    ) {
    window.onresize = (e) => {
      this.ngZone.run(() => {
        this.innerHeight = window.innerHeight + "px";
      });
    };
    this.getScreenHeight();
  }

  ngOnInit() {
    /*
    if(this.keycloakSecurityService.isEntrepriseManager()){
      this.router.navigateByUrl("/entreprise/dashboard/admin");
    }
    else 
    if(this.keycloakSecurityService.isEntrepriseHR()){
      this.router.navigateByUrl("/entreprise/dashboard/hr");
    }
    */
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + "px";
  }
}
