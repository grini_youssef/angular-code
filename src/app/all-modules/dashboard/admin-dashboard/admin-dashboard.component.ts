import { Component, OnInit } from "@angular/core";
import { Entreprise } from "src/app/models/entreprise";
import { EntrepriseService } from "src/app/services/entreprise.service";
import { KeycloakSecurityService } from "src/app/services/keycloak-security.service";
import { MessageService } from "src/app/services/message.service";

@Component({
  selector: "app-admin-dashboard",
  templateUrl: "./admin-dashboard.component.html",
  styleUrls: ["./admin-dashboard.component.css"],
})
export class AdminDashboardComponent implements OnInit {
  public chartData;
  public chartOptions;
  public lineData;
  public lineOption;
  public barColors = {
    a: "#007bff",
    b: "#6610f2",
  };
  public lineColors = {
    a: "#007bff",
    b: "#6610f2",
  };

  userInformations: any;
  id_entreprise: any;
  entreprise = new  Entreprise();
  errorMessage: string = null;

  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private entrepriseService: EntrepriseService,
    private messageService : MessageService,
  ) { }

  ngOnInit() {
    this.userInformations = this.keycloakSecurityService.userInformations;
    this.id_entreprise = this.userInformations.entreprise;
    this.getEntreprise();

    this.chartOptions = {
      xkey: "y",
      ykeys: ["a", "b"],
      labels: ["Series A", "Series B"],
      barColors: [this.barColors.a, this.barColors.b],
    };

    this.chartData = [
      { y: "2006", a: 100, b: 90 },
      { y: "2007", a: 75, b: 65 },
      { y: "2008", a: 50, b: 40 },
      { y: "2009", a: 75, b: 65 },
      { y: "2010", a: 50, b: 40 },
      { y: "2011", a: 75, b: 65 },
      { y: "2012", a: 100, b: 90 },
    ];

    this.lineOption = {
      xkey: "y",
      ykeys: ["a", "b"],
      labels: ["Series A", "Series B"],
      resize: true,
      lineColors: [this.lineColors.a, this.lineColors.b],
    };

    this.lineData = [
      { y: "2006", a: 100, b: 90 },
      { y: "2007", a: 75, b: 65 },
      { y: "2008", a: 50, b: 40 },
      { y: "2009", a: 75, b: 65 },
      { y: "2010", a: 50, b: 40 },
      { y: "2011", a: 75, b: 65 },
      { y: "2012", a: 100, b: 90 },
    ];
  }

  getEntreprise() {

    this.entrepriseService
        .getEntreprise(this.id_entreprise)
        .subscribe(
          data => {
            this.entreprise = data;
          },
          err => {
            this.errorMessage = err;
            console.log('errorrr ! ', err)
          })
  }

  updateEntreprise() {
    this.entrepriseService
        .UpdateEntreprise(this.entreprise.id, this.entreprise)
        .subscribe(
          response => {
            this.messageService.showMessage("div#msg","alert-info","entreprise has been successfully updated.", "glyphicon-ok");
          },
          err => {
            this.errorMessage = err;
            console.log('error updating entreprise ! ', err)
          }
        );
  }

}
