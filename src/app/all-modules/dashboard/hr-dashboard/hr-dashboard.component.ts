import { Component, OnInit } from '@angular/core';
import { Entreprise } from 'src/app/models/entreprise';
import { EntrepriseService } from 'src/app/services/entreprise.service';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
import { MessageService } from 'src/app/services/message.service';
import { OfferService } from 'src/app/services/offer.service';

@Component({
  selector: 'app-hr-dashboard',
  templateUrl: './hr-dashboard.component.html',
  styleUrls: ['./hr-dashboard.component.css']
})
export class HrDashboardComponent implements OnInit {

  public chartData;
  public chartOptions;
  public lineData;
  public lineOption;
  public barColors = {
    a: "#007bff",
    b: "#6610f2",
  };
  public lineColors = {
    a: "#007bff",
    b: "#6610f2",
  };

  userInformations: any;
  id_entreprise: any;
  entreprise = new  Entreprise();
  errorMessage: string = null;
  public offers: any = [];
  public offers2: any = [];
  public NumberOfOpenedOffers : number = 0;
  public NumberOfCandidatures : number = 0;
  public NumberOfInterviewsCampaigns : number = 0;

  public scoresOver80 : Array<Number> = [];
  public statusApproved : Array<Number> = [];
  public candidatsHired : Array<Number> = [];
  public statusApproved2 : Array<Number> = [];


  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private entrepriseService: EntrepriseService,
    private messageService : MessageService,
    private offerService : OfferService,
  ) { }

  ngOnInit() {
    this.userInformations = this.keycloakSecurityService.userInformations;
    this.id_entreprise = this.userInformations.entreprise;
    this.getOffers(); 
    this.getEntreprise();

    this.chartOptions = {
      xkey: "y",
      ykeys: ["a", "b"],
      labels: ["Series A", "Series B"],
      barColors: [this.barColors.a, this.barColors.b],
    };

    this.chartData = [
      { y: "2006", a: 100, b: 90 },
      { y: "2007", a: 75, b: 65 },
      { y: "2008", a: 50, b: 40 },
      { y: "2009", a: 75, b: 65 },
      { y: "2010", a: 50, b: 40 },
      { y: "2011", a: 75, b: 65 },
      { y: "2012", a: 100, b: 90 },
    ];

    this.lineOption = {
      xkey: "y",
      ykeys: ["a", "b"],
      labels: ["Series A", "Series B"],
      resize: true,
      lineColors: [this.lineColors.a, this.lineColors.b],
    };

    this.lineData = [
      { y: "2006", a: 100, b: 90 },
      { y: "2007", a: 75, b: 65 },
      { y: "2008", a: 50, b: 40 },
      { y: "2009", a: 75, b: 65 },
      { y: "2010", a: 50, b: 40 },
      { y: "2011", a: 75, b: 65 },
      { y: "2012", a: 100, b: 90 },
    ];
  }

  getEntreprise() {

    this.entrepriseService
        .getEntreprise(this.id_entreprise)
        .subscribe(
          data => {
            this.entreprise = data;
          },
          err => {
            this.errorMessage = err;
            console.log('errorrr ! ', err)
          })
  }

  getOffers() {
    this.offerService.getOffers(this.id_entreprise).subscribe(
          data => {
            this.offers = data;
            this.offers = this.offers.filter(o => o.status == "open");
            this.NumberOfOpenedOffers = this.offers.length;
            for(const offer of this.offers){
              this.NumberOfCandidatures = this.NumberOfCandidatures + offer["candidatures"].length;
              
              let i = 0;
              let j = 0;
              for(const candidature of offer["candidatures"]){
                if(candidature["score"]>=80){
                  i++;
                }
                if(candidature["status"] == "approved"){
                  j++;
                }
              }
              this.scoresOver80.push(i);
              this.statusApproved.push(j);

            }

            this.offers2 = this.offers.filter(o => o.workflowInterview.length > 0);
            this.NumberOfInterviewsCampaigns = this.offers2.length;
            for(const offr of this.offers2){
              let k = 0;
              let h = 0;
              for(const candidat of offr["candidatures"]){
                if(candidat["hiring"] == "yes"){
                  k++;
                }
                if(candidat["status"] == "approved"){
                  h++;
                }
              }
              this.candidatsHired.push(k);
              this.statusApproved2.push(h);
            }

          },
          err => {
            this.errorMessage = err;
            console.log('error getting offers ! ', err)
          })
  }

  updateEntreprise() {
    this.entrepriseService
        .UpdateEntreprise(this.entreprise.id, this.entreprise)
        .subscribe(
          response => {
            this.messageService.showMessage("div#msg","alert-info","entreprise has been successfully updated.", "glyphicon-ok");
          },
          err => {
            this.errorMessage = err;
            console.log('error updating entreprise ! ', err)
          }
        );
  }

}
