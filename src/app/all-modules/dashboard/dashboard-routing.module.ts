import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard.component";
import { AdminDashboardComponent } from "./admin-dashboard/admin-dashboard.component";
import { EmployeeDashboardComponent } from "./employee-dashboard/employee-dashboard.component";
import { HrDashboardComponent } from "./hr-dashboard/hr-dashboard.component";
import { AfterLoginEntrepriseHrAcceptedService } from "src/app/services/guards/after-login-entreprise-hr-accepted.service";

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    children: [
      {path: '', redirectTo: 'hr', pathMatch:'full'},
      { 
        path: "admin", 
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component: AdminDashboardComponent 
      },
      { 
        path: "hr", 
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component: HrDashboardComponent 
      },
      { 
        path: "employee", 
        canActivate: [AfterLoginEntrepriseHrAcceptedService],
        component: EmployeeDashboardComponent 
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
