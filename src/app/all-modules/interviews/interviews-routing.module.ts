import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/services/guards/auth.guard';
import { InterviewByOfferComponent } from './interview-by-offer/interview-by-offer.component';
import { InterviewsComponent } from './interviews.component';
import { ListOffersComponent } from './list-offers/list-offers.component';
import { WorkflowInterviewComponent } from './workflow-interview/workflow-interview.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: 'list-offers',
    pathMatch: 'full'
  },
  {
    path:"",
    component: InterviewsComponent,
    children:[
      {
        path:"list-offers",
        component: ListOffersComponent,
        canActivate: [AuthGuard],
      },
      {
        path:"interviews-by-offer/:id",
        component:InterviewByOfferComponent,
        canActivate: [AuthGuard],
      },
      {
        path:"workflow-interviews/:id",
        component:WorkflowInterviewComponent,
        canActivate: [AuthGuard],
      }
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterviewsRoutingModule { }
