import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { InterviewsRoutingModule } from './interviews-routing.module';
import { InterviewsComponent } from './interviews.component';
import { InterviewByOfferComponent } from './interview-by-offer/interview-by-offer.component';
import { ListOffersComponent } from './list-offers/list-offers.component';
import { WorkflowInterviewComponent } from './workflow-interview/workflow-interview.component';
import { SharingModule } from 'src/app/sharing/sharing.module';


@NgModule({
  declarations: [InterviewsComponent, InterviewByOfferComponent, ListOffersComponent, WorkflowInterviewComponent],
  imports: [
    CommonModule,
    InterviewsRoutingModule,
    DataTablesModule,
    FormsModule,
    BsDatepickerModule.forRoot(),
    NgbModule,
    SharingModule
  ]
})
export class InterviewsModule { }
