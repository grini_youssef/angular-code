import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Offer } from 'src/app/models/offer';
import { User } from 'src/app/models/user';
import { WorkflowInterview } from 'src/app/models/workflow-interview';
import { EntrepriseService } from 'src/app/services/entreprise.service';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
import { InterviewService } from '../interview.service';

@Component({
  selector: 'app-workflow-interview',
  templateUrl: './workflow-interview.component.html',
  styleUrls: ['./workflow-interview.component.css']
})
export class WorkflowInterviewComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private interviewService: InterviewService,
    private keycloakSecurityService : KeycloakSecurityService,
    private entrepriseService : EntrepriseService,
    private toastr: ToastrService,
  ) { }

  theOfferId = this.route.snapshot.params['id'];
  offer: Offer = new Offer();
  workflows: WorkflowInterview[] = [];
  id_entreprise : number;
  allUsers: User[] = [];

  ngOnInit(): void {
    this.getWorkflowInterviews();
    this.getOffer();
    this.id_entreprise = this.keycloakSecurityService.userInformations.entreprise;
    this.listUsersOfEntreprise();
  }
  // show add-workflow and hide main page
  showMainPage: boolean = true;
  showAddWorkFlow: boolean = false;
  firstData:any = [];
  showAddWorkflow(){
    this.showAddWorkFlow = true;
    this.showMainPage = false;
    if(this.workflows.length==0){
      const workflow = new WorkflowInterview();
        this.workflows.push(workflow);
    }
  }

  // get selected offer
  getOffer(){
    this.interviewService.getOffer(this.theOfferId).subscribe(
      data=>{
        this.offer = data;
      },
      err=>{
        console.log('error generated'+ err);
      }
    )
  }

  // get workflow interviews
  getWorkflowInterviews(){
    this.interviewService.getWorkflowInterviews(this.theOfferId).subscribe(
      data=>{
        this.workflows = data;
        this.firstData = data;
      },
      err=>{
        console.log('error generated'+ err);
      }
    )
  }

  // get all user the entreprise
  listUsersOfEntreprise(){
    this.entrepriseService.getEntrepriseUsers(this.id_entreprise).subscribe(
      data => {
        this.allUsers = data;
      },
      err => {
        console.log('error generated'+ err);
      }
    )}

  // add new row of interview
  addInterviewItem(){
    var itemInterview = new WorkflowInterview();
    this.workflows.push(itemInterview);
  }
  removeInterviewItem(itemInterview: WorkflowInterview){
    const index: number = this.workflows.indexOf(itemInterview);
    if (index != -1) {
        this.workflows.splice(index, 1);
    }  
  }

  // add or edit workflow interviews
  addOrEditWorkflow(){
    this.workflows.forEach(itemInterview => {
      const evaluator: User = this.allUsers.find(u=>u.id == itemInterview.evaluatorId);
      itemInterview.evaluatorEmail = evaluator.email;
      itemInterview.evaluatorName = evaluator.firstName +" "+ evaluator.lastName;
    });
    console.log(this.workflows);
    this.interviewService.addOrEditWorkflowInterviews(this.workflows, this.theOfferId).subscribe(
      data=>{
        this.getWorkflowInterviews();
        this.showAddWorkFlow = false;
        this.showMainPage = true;
        this.toastr.success("Le workflow a été modifié avec succès");
      },
      err=>{
        console.log('error generated'+ err);
        this.toastr.error("une erreur a été generée, essayer plus tard");
      }
    )
  }


}
