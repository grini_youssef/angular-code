import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home.component";
import { EntrepriseHomeComponent } from "./entreprise-home/entreprise-home.component";
import { EntrepriseInfosComponent } from "./entreprise-infos/entreprise-infos.component";
import { AfterLoginEntrepriseManagerService } from "src/app/services/guards/after-login-entreprise-manager.service";
import { AfterLoginEntrepriseHrService } from "src/app/services/guards/after-login-entreprise-hr.service";

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    children: [
      { path: "entreprise-home", component: EntrepriseHomeComponent },
      { path: "entreprise-infos", canActivate: [AfterLoginEntrepriseManagerService], component: EntrepriseInfosComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
