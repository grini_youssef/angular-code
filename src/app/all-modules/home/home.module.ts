import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { EntrepriseHomeComponent } from './entreprise-home/entreprise-home.component';
import { EntrepriseInfosComponent } from './entreprise-infos/entreprise-infos.component';
import { MorrisJsModule } from 'angular-morris-js';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharingModule } from 'src/app/sharing/sharing.module';

@NgModule({
  declarations: [HomeComponent, EntrepriseHomeComponent, EntrepriseInfosComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MorrisJsModule,
    FormsModule,
    ReactiveFormsModule,
    SharingModule

  ]
})
export class HomeModule { }
