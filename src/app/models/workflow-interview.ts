import { Offer } from "./offer";

export class WorkflowInterview {
   id: number;
   titleInteview: string;
   evaluatorId: string;
   evaluatorEmail: string;
   evaluatorName: string;
   offer: Offer;
}
