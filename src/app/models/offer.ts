export class Offer{
    id: number;
    title: string;
    domaine: string;
    country: string;
    city: string;
    postalCode: string;
    address: string;
    experience: number;
    salaryFrom: number;
    salaryTo: number;
    type: string;
    status: string;
    startDate: Date;
    expiredDate: Date;
    description: string;
    createdDate: Date;
}