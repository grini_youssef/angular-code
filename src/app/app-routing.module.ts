/*
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AfterLoginEntrepriseHrService } from './services/guards/after-login-entreprise-hr.service';
import { CampaignComponent } from './campaign/campaign.component';




const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch:'full'},
  {path: 'home', component:HomeComponent},
  {path: 'campaign/:id', canActivate: [AfterLoginEntrepriseHrService], component:CampaignComponent},

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule { }
*/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path: 'login', loadChildren: () => import(`./login/login.module`).then(m => m.LoginModule) },
  { path: 'sign-up', loadChildren: () => import(`./sign-up/sign-up.module`).then(m => m.SignUpModule) },
  { path: 'error', loadChildren: () => import(`./errorpages/errorpages.module`).then(m => m.ErrorpagesModule) },
  { path: 'entreprise', loadChildren: () => import(`./all-modules/all-modules.module`).then(m => m.AllModulesModule) },
  //{ path: '**', loadChildren: () => import(`./login/login.module`).then(m => m.LoginModule) },
  //{path: '**', redirectTo: 'home', pathMatch:'full'},
  {path: '', redirectTo: 'home', pathMatch:'full'},
  {path: 'home', component:HomeComponent},

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
