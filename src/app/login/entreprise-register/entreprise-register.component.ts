import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Entreprise } from 'src/app/models/entreprise';
import { User } from 'src/app/models/user';
import { EntrepriseService } from 'src/app/services/entreprise.service';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-entreprise-register',
  templateUrl: './entreprise-register.component.html',
  styleUrls: ['./entreprise-register.component.css']
})
export class EntrepriseRegisterComponent implements OnInit {

  entreprise = new Entreprise();
  user = new User();

  public form = {
    password_confirmation: null
  };

  entrepriseForm : boolean = true;
  adminForm : boolean = false;

  constructor(
    private entrepriseService : EntrepriseService,
    private keycloakSecurityService: KeycloakSecurityService,
    private router : Router,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
  }

  onSubmit(){
    this.toastr.info("Wait ...","Info",{
      disableTimeOut: true,
      tapToDismiss: true,
      closeButton: true,
    });
    this.entreprise.statut = "in progress";
    this.user.email = this.entreprise.email;
    this.entrepriseService.CreateEntreprise(this.entreprise, this.user).subscribe(
      data => {
        console.log(data);
        this.toastr.clear();
        this.toastr.success("you have signed up successfully", "Success");
        this.onLogin();

      },
      err => {
        console.log('errorrr ! ', err);
        this.toastr.clear();
        this.toastr.warning(err.error.message, "Warning!");
      }
    );

  }


  onLogin(){
    this.keycloakSecurityService.login();
  }

  next(){
    this.entrepriseForm = false;
    this.adminForm = true;
  }

  back(){
    this.entrepriseForm = true;
    this.adminForm = false;
  }

}
