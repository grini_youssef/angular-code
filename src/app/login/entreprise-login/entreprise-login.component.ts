import { Component, OnInit } from '@angular/core';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
declare const $: any;

@Component({
  selector: 'app-entreprise-login',
  templateUrl: './entreprise-login.component.html',
  styleUrls: ['./entreprise-login.component.css']
})
export class EntrepriseLoginComponent implements OnInit {


  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
  ) { }

  ngOnInit() {
    //$("#connecting").modal("show");
    this.keycloakSecurityService.keycloak.login();
  }


}
