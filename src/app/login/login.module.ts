import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { LoginRoutingModule } from './login-routing.module';
import { ForgotComponent } from './forgot/forgot.component';
import { RegisterComponent } from './register/register.component';
import { OtpComponent } from './otp/otp.component';
import { LockscreenComponent } from './lockscreen/lockscreen.component';
import { JobListComponent } from './job-list/job-list.component';
import { JobViewComponent } from './job-view/job-view.component';
import { EntrepriseRegisterComponent } from './entreprise-register/entreprise-register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EntrepriseLoginComponent } from './entreprise-login/entreprise-login.component';

@NgModule({
  declarations: [LoginComponent,EntrepriseLoginComponent, ForgotComponent,EntrepriseRegisterComponent, RegisterComponent, OtpComponent, LockscreenComponent, JobListComponent, JobViewComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LoginModule { }
