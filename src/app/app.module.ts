
import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, DoBootstrap, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { KeycloakSecurityService } from './services/keycloak-security.service';
import { RequestInterceptorService } from './services/request-interceptor.service';
import { BeforeLoginEntrepriseService } from './services/guards/before-login-entreprise.service';
import { AfterLoginEntrepriseManagerService } from './services/guards/after-login-entreprise-manager.service';
import { MessageService } from './services/message.service';
import { AfterLoginEntrepriseManagerAcceptedService } from './services/guards/after-login-entreprise-manager-accepted.service';
import { EntrepriseConfigService } from './services/entreprise-config.service';
import { EntrepriseService } from './services/entreprise.service';
import { RoleService } from './services/role.service';
import { AfterLoginEntrepriseHrService } from './services/guards/after-login-entreprise-hr.service';
import { HeaderService } from './services/header.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// Bootstrap DataTable
import { DataTablesModule } from 'angular-datatables';
import { ToastrModule } from 'ngx-toastr';
import { AfterLoginEntrepriseHrAcceptedService } from './services/guards/after-login-entreprise-hr-accepted.service';
import { ParsingService } from './services/parsing.service';
import { OfferService } from './services/offer.service';
import * as $ from 'jquery';
import * as bootstrap from "bootstrap";
import { CandidatureService } from './services/candidature.service';
import { CritereService } from './services/critere.service';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

export function keycloakFactory(keycloakSecurityService: KeycloakSecurityService) {
   return () => keycloakSecurityService.init();
 }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

const keycloakSecurityService = new KeycloakSecurityService();

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    DataTablesModule,
    ToastrModule.forRoot(
      {
        timeOut: 2500,
        positionClass: 'toast-bottom-right',
        preventDuplicates: true,
      }
    ),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    }
    )
  ],
  exports: [TranslateModule],
  providers: [
    BeforeLoginEntrepriseService,
    AfterLoginEntrepriseManagerService,
    AfterLoginEntrepriseManagerAcceptedService,
    AfterLoginEntrepriseHrAcceptedService,
    EntrepriseService,
    RoleService,
    MessageService,
    EntrepriseConfigService,
    AfterLoginEntrepriseHrService,
    ParsingService,
    OfferService,
    CandidatureService,
    CritereService,
    HeaderService,
    HttpClient,
   // { provide: APP_INITIALIZER, deps: [KeycloakSecurityService], useFactory: keycloakFactory, multi: true },
    { provide: KeycloakSecurityService, useValue: keycloakSecurityService },
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptorService, multi: true }
  ],
  entryComponents: [AppComponent],
  //bootstrap: [AppComponent]
})

export class AppModule implements DoBootstrap {
  ngDoBootstrap(appRef: import("@angular/core").ApplicationRef): void {
    keycloakSecurityService.init().then(data => {
      console.log('authenticated + token :', data);
      appRef.bootstrap(AppComponent);

    }).catch(err => {
      console.error('err', err);
    });
  }
}

