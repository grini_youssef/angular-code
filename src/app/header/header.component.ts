import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { EntrepriseConfigService } from "../services/entreprise-config.service";
import { KeycloakSecurityService } from "../services/keycloak-security.service";
import { HeaderService } from "./header.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {

  isAuth = false;
  keycloak: any;
  userInformations: any;
  isAccepted: boolean = true;

  
  jsonData: any = {
    notification: [],
    message: [],
  };
  notifications: any;
  messagesData: any;

  constructor(
    private headerService: HeaderService, 
    private router: Router,
    private keycloakSecurityService: KeycloakSecurityService,
    private entrepriseConfigService : EntrepriseConfigService,
    ) {}

  ngOnInit() {

    this.keycloak = this.keycloakSecurityService.keycloak;
    this.isAuth = this.keycloak.authenticated;
    this.userInformations = this.isAuth ? this.keycloak.idTokenParsed : {};

    if(this.isAuth){
      this.entrepriseConfigService.init()
      .then(data => {
        this.isAccepted = (data == "accepted") ;
  
      }).catch(err => {
        console.error('err', err);
        this.isAccepted = false;
      });
    } 

    // this.getDatas("notification");
    // this.getDatas("message");

    this.notifications = [
      {
        message: "Patient appointment booking",
        author: "John Doe",
        function: "added new task",
        time: "4 mins ago",
      },
      {
        message: "Patient appointment booking",
        author: "John Doe",
        function: "added new task",
        time: "1 hour ago",
      },
      {
        message: "Patient appointment booking",
        author: "John Doe",
        function: "added new task",
        time: "4 mins ago",
      },
      {
        message: "Patient appointment booking",
        author: "John Doe",
        function: "added new task",
        time: "1 hour ago",
      },
      {
        message: "Patient appointment booking",
        author: "John Doe",
        function: "added new task",
        time: "4 mins ago",
      },
      {
        message: "Patient appointment booking",
        author: "John Doe",
        function: "added new task",
        time: "1 hour ago",
      },
    ];

    this.messagesData = [
      {
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing",
        author: "Mike Litorus",
        time: "4 mins ago",
      },
      {
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing",
        author: "Mike Litorus",
        time: "1 hour ago",
      },
      {
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing",
        author: "Mike Litorus",
        time: "4 mins ago",
      },
      {
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing",
        author: "Mike Litorus",
        time: "1 hour ago",
      },
      {
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing",
        author: "Mike Litorus",
        time: "4 mins ago",
      },
      {
        message: "Lorem ipsum dolor sit amet, consectetur adipiscing",
        author: "Mike Litorus",
        time: "1 hour ago",
      },
    ];

    this.selectedLang = localStorage.getItem('selectedLang') || 'fr';
    $('#language img').attr('src', 'assets/img/flags/'+this.selectedLang+'.png');
    let nameLang = this.selectedLang == 'fr' ? 'Français' : 'English';
    $('#language span').text(nameLang);

  }

  getDatas(section) {
    this.headerService.getDataFromJson(section).subscribe((data) => {
      this.jsonData[section] = data;
    });
  }

  clearData(section) {
    this.jsonData[section] = [];
  }
  onSubmit() {
    this.router.navigate(["/pages/search"]);
  }

  onLogin() {
    this.keycloak.login();
  }

  onLogout() {
    this.keycloak.logout();
  }

  ManagedAccount() {
    this.keycloak.accountManagement();
  }

  isEntrepriseManager() {
    return this.keycloak.hasRealmRole('entreprise-manager');
  }

  isEntrepriseHR() {
    return this.keycloak.hasRealmRole('hr-assistant');
  }

  selectedLang;
  changeLang(locale:string, lang:string ){
    $('#language img').attr('src', 'assets/img/flags/'+locale+'.png');
    $('#language span').text(lang);
    this.selectedLang = locale;
    localStorage.setItem('selectedLang', locale);
    window.location.reload();
  }

}
