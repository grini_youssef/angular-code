import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Entreprise } from '../models/entreprise';
import { User } from '../models/user';
import { Role } from '../models/role';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {

  headers: HttpHeaders =new HttpHeaders;
  options : any;
  private baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient, 
    ) { 
      this.headers.append('enctype', 'multipart/form-data');
      this.headers.append('Content-Type', 'application/json');
      this.headers.append('X-requested-with', 'XMLHttpRequest');
      this.options = {headers: this.headers};
    }

    public getEntreprises() {
      return this.http.get<Entreprise[]>(`${this.baseUrl}/getEntreprises`, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getEntreprise(id) {
      return this.http.get<Entreprise>(`${this.baseUrl}/getEntreprise/`+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public CreateEntreprise(entreprise, user) {
      return this.http.post<any>(`${this.baseUrl}/createEntreprise`, {entreprise, user}, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        ); 
    }


    public CreateEntrepriseAdmin(entreprise, user) {
      return this.http.post<any>(`${this.baseUrl}/createEntrepriseAdmin`, {entreprise, user}, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public UpdateEntreprise(id : number, entreprise : Entreprise) {
      return this.http.put<any>(`${this.baseUrl}/updateEntreprise/`+id, entreprise, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public DeleteEntreprise(id : number) {
      return this.http.delete<any>(`${this.baseUrl}/deleteEntreprise/`+id, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public CreateEntrepriseUser(user : User, entreprise : Entreprise, roles : Array<any>) {
      return this.http.post<any>(`${this.baseUrl}/createEntrepriseUser`, {user, entreprise, roles}, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getUser(id) {
      return this.http.get<User>(`${this.baseUrl}/getUser/`+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public UpdateUser(id : string, user : User, entreprise : Entreprise, roles : Array<any>) {
      return this.http.put<any>(`${this.baseUrl}/updateEntrepriseUser/`+id, {user, entreprise, roles}, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getEntrepriseUsers(id) {
      return this.http.get<User[]>(`${this.baseUrl}/getEntrepriseUsers/`+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public DeleteUser(id : string) {
      return this.http.delete<any>(`${this.baseUrl}/deleteUser/`+id, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

  
  
  
    private handleError(errorRes: HttpErrorResponse) {
      console.log('errorRes', errorRes)
      /*
      let errorMessage = 'an unknown error occured';
      if (!errorRes.error || !errorRes.error.error || !errorRes.error.message) {
        return throwError(errorMessage);
      }
      switch (errorRes.error.message) {
        case 'Forbidden':
          errorMessage = 'Vous n\'avez pas les droits ! ou vous n\'êtes pas logger';
          break;
        case 'EMAIL_NOT_FOUND':
          errorMessage = 'this Email does not exist';
          break;
        case 'INVALID_PASSWORD':
          errorMessage = 'this password is not correct';
          break;
  
      }
      */
      return throwError(errorRes);
    }
}
