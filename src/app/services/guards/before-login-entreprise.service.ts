import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Entreprise } from 'src/app/models/entreprise';
import { EntrepriseService } from '../entreprise.service';
import { KeycloakSecurityService } from '../keycloak-security.service';

@Injectable()
export class BeforeLoginEntrepriseService implements CanActivate {

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if(!this.keycloakSecurityService.isAuth) {
      return true;
    }
    else {
      if(this.keycloakSecurityService.isEntrepriseManager() || this.keycloakSecurityService.isEntrepriseHR()){
        return new Promise((resolve) => {
          this.entrepriseService
          .getEntreprise(this.keycloakSecurityService.userInformations.entreprise)
          .subscribe(
            data => {
              this.entreprise = data;
              if(this.entreprise.statut == "accepted"){
                this.router.navigateByUrl('entreprise/dashboard');
                resolve(false);
              }
              else{
                if(this.keycloakSecurityService.isEntrepriseManager()){
                  this.router.navigateByUrl('entreprise/home/entreprise-infos');
                  resolve(false);
                }
                else if(this.keycloakSecurityService.isEntrepriseHR()){
                  this.router.navigateByUrl('entreprise/home/entreprise-home');
                  resolve(false);
                }
              }
            },
            err => {
              this.router.navigateByUrl('entreprise/home/entreprise-home');
              resolve(false);
            })
          });
      }
      else{
        this.router.navigateByUrl('');
        return false;
      }
          
    }
  }
  entreprise = new Entreprise();
  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private router: Router,
    private entrepriseService : EntrepriseService
    ) { }

}
