import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { KeycloakSecurityService } from '../keycloak-security.service';

@Injectable()
export class AfterLoginEntrepriseHrService implements CanActivate {
  

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if(this.keycloakSecurityService.isAuth) {
      if(this.keycloakSecurityService.isEntrepriseHR()){
        return true;
      }
      else{
        this.router.navigateByUrl('');
         return false;
      }
    }
    else {
         this.router.navigateByUrl('entreprise/home/entreprise-home');
         return false;
    }
  }
  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private router: Router
    ) { }

}
