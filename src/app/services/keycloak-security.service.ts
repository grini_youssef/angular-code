import { Injectable } from '@angular/core';
import { KeycloakInstance } from 'keycloak-js';
import { environment } from 'src/environments/environment';
import { EntrepriseService } from './entreprise.service';
declare var Keycloak: any;

@Injectable({
  providedIn: 'root'
})
export class KeycloakSecurityService {

  public keycloak: KeycloakInstance;
  public userInformations: any;
  isAuth = false;

  constructor() { }

  init() {
    return new Promise((resolve, reject) => {
      console.log('INIT : Service keycloak security ');
      this.keycloak = new Keycloak({
        url: environment.keycloak_url,
        realm: 'recrute-realm',
        clientId: 'recrutement-front'
      });
      this.keycloak.init({
        // onLoad: 'login-required'
        onLoad: 'check-sso'
        //promiseType: 'native'

      }).then((authenticated) => {
        this.isAuth = this.keycloak.authenticated;
        this.userInformations = this.isAuth ? this.keycloak.idTokenParsed : {};
        resolve({ authenticated, token: this.keycloak.token })
      }).catch(err => {
        reject(err);
      });
    });


  }

  getToken(){
    return this.keycloak.token;
  }

  login(){
    return this.keycloak.login();
  }

  isAppManager() {
    return this.keycloak.hasRealmRole('app-manager');
  }

  isEntrepriseManager() {
    return this.keycloak.hasRealmRole('entreprise-manager');
  }

  isEntrepriseHR() {
    return this.keycloak.hasRealmRole('hr-assistant');
  }


}