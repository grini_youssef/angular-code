import { Injectable, OnInit } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';
import { Entreprise } from '../models/entreprise';
import { EntrepriseService } from './entreprise.service';
import { KeycloakSecurityService } from './keycloak-security.service';

@Injectable({
  providedIn: 'root'
})
export class EntrepriseConfigService implements OnInit{


  errorMessage : any;
  entreprise = new Entreprise();
  isAccepted: boolean;

  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private entrepriseService: EntrepriseService
  ) { }
  ngOnInit(): void {
    this.isEntrepriseAccepted();
  }

  getEntreprise() {
     this.entrepriseService
        .getEntreprise(this.keycloakSecurityService.userInformations.entreprise)
        .subscribe(
          data => {
            this.entreprise = data;
          },
          err => {
            this.errorMessage = err;
            console.log('errorrr ! ', err)
          })
          
  }


  isEntrepriseAccepted(){
    this.init().then(data => {
      this.isAccepted = (data == "accepted") ;

    }).catch(err => {
      console.error('err', err);
      this.isAccepted = false;
    });
  }



  init() {
    return new Promise((resolve, reject) => {

      this.entrepriseService
      .getEntreprise(this.keycloakSecurityService.userInformations.entreprise)
      .subscribe(
        data => {
          this.entreprise = data;
          resolve(this.entreprise.statut);
        },
        err => {
          this.errorMessage = err;
          console.log('errorrr ! ', err);
          reject(err);
        })

    });
  }



}