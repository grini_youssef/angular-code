import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Offer } from '../models/offer';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  headers: HttpHeaders =new HttpHeaders;
  options : any;
  private baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
    ) { 
      this.headers.append('enctype', 'multipart/form-data');
      this.headers.append('Content-Type', 'application/json');
      this.headers.append('X-requested-with', 'XMLHttpRequest');
      this.options = {headers: this.headers};
    }

    public getOffers(id_entreprise) {
      return this.http.get<Offer[]>(`${this.baseUrl}/GetOffers/`+id_entreprise, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getOffer(id) {
      return this.http.get<Offer>(`${this.baseUrl}/GetOffer/`+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public CreateOffer(offer, id_entreprise) {
      return this.http.post<any>(`${this.baseUrl}/createOffer/`+id_entreprise, offer, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public UpdateOffer(id, offer) {
      return this.http.put<any>(`${this.baseUrl}/updateOffer/`+id, offer, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public DeleteOffer(id : number) {
      return this.http.delete<any>(`${this.baseUrl}/deleteOffer/`+id, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

  
  
  
    private handleError(errorRes: HttpErrorResponse) {
      console.log('errorRes', errorRes)
      /*
      let errorMessage = 'an unknown error occured';
      if (!errorRes.error || !errorRes.error.error || !errorRes.error.message) {
        return throwError(errorMessage);
      }
      switch (errorRes.error.message) {
        case 'Forbidden':
          errorMessage = 'Vous n\'avez pas les droits ! ou vous n\'êtes pas logger';
          break;
        case 'EMAIL_NOT_FOUND':
          errorMessage = 'this Email does not exist';
          break;
        case 'INVALID_PASSWORD':
          errorMessage = 'this password is not correct';
          break;
  
      }
      */
      return throwError(errorRes);
    }
}
